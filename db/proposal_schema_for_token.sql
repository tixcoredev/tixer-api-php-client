CREATE TABLE `api_auth` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
 `fb_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
 `token` text COLLATE utf8_unicode_ci NOT NULL,
 `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
 `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
 PRIMARY KEY (`id`),
 UNIQUE KEY `api_auth_email_unique` (`email`),
 UNIQUE KEY `api_auth_fb_token_unique` (`fb_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci