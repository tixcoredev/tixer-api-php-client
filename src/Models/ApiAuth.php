<?php
/**
 * Tixer Api
 *
 * PHP version 5.6
 *
 * @category   Tixer Api
 * @package    Tixer
 * @subpackage TixerApiClient/Models
 * @author     Dariusz Wiśniewski <kontakt@darks.pl>
 * @license    http://tixer.pl/ MIT
 * @link       http://tixer.pl/
 */
namespace TixerApiClient\Models;

/**
 * ApiAuth class
 */
class ApiAuth extends ApiModel
{
    protected $attributes = ['token', 'roles'];

    /**
     * This method performs an api login operation with a regular credentials like
     * login and password.
     * API operation: /Auth/login (post)
     * @param String $email
     * @param String $password
     * @return ApiAuth $this
     */
    public function login($email, $password)
    {
        $this->attributes['token'] = $this->apiClient->post('/Auth/login', ['email' => $email, 'password' => $password]);
        return $this;
    }

    /**
     * This method performs an api login operation based on Facebook token.
     * API operation: /Auth/login (post)
     * @param String $fbToken
     * @return ApiAuth $this
     */
    public function loginByFacebook($fbToken)
    {
        $this->attributes['token'] = $this->apiClient->post('/Auth/login', ['fb_token' => $fbToken]);
        return $this;
    }

    /**
     * This method returns available roles.
     * API operation: /Auth/roles (get)
     * @return Array
     */
    public function roles()
    {
        return $this->attributes['roles'] = $this->apiClient->get('/Auth/roles', true);
    }
}
