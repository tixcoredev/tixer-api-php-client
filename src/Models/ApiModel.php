<?php
/**
 * Tixer Api
 *
 * PHP version 5.6
 *
 * @category   Tixer Api
 * @package    Tixer
 * @subpackage TixerApiClient/Models
 * @author     Dariusz Wiśniewski <kontakt@darks.pl>
 * @license    http://tixer.pl/ MIT
 * @link       http://tixer.pl/
 */
namespace TixerApiClient\Models;

use TixerApiClient\ApiValidator;

/**
 * ApiModel class
 */
class ApiModel
{
    protected $apiClient;
    protected $attributes = [];
    protected $sub;
    protected $rules;
    protected $validator;

    /**
     * This contructor is a base for all API related models.
     * It requires valid ApiClient object to perform remote
     * requests. It's also allowed to set the attributes while creating an object.
     * @param ApiClient $apiClient
     * @return Void
     */
    public function __construct($apiClient, $attributes = [])
    {
        $this->apiClient = $apiClient;
        $this->attributes = $attributes;
        $this->validator = new ApiValidator();
    }

    /**
     * Getter that uses an attributes array to get values.
     * @param String $attribute
     * @return Mixed
     */
    public function __get($attribute)
    {
        return array_key_exists($attribute, $this->attributes) ? $this->attributes[$attribute] : null;
    }

    /**
     * Setter that uses an attributes array to keep values.
     * @param String $attribute
     * @param Mixed $value
     * @return Void
     */
    public function __set($attribute, $value)
    {
        if (array_key_exists($attribute, $this->attributes)) {
            $this->attributes[$attribute] = $value;
        }
    }

    /**
     * This method allow massive assignment of model's attributes. This function
     * uses array_merge function to override existing attributes and keep all
     * others.
     * @param Array $attributes
     * @return Void
     */
    public function setAttributes($attributes)
    {
        if (is_array($atrributes)) {
            $this->attributes = array_merge($this->attributes, $attributes);
        }
    }

    /**
     * This method allows to set sub claim in case you would like to perform
     * an operation in behalf of someone else. Example: BOW user places an
     * order for a client.
     * @param numeric $sub
     * @return Void
     */
    public function setSub($sub)
    {
        $this->sub = $sub;
    }

    /**
     * This method returns all atributes in json format.
     * @return JSON
     */
    public function toJson()
    {
        return json_encode($this->attributes);
    }

    /**
     * This method returns all attributes in array format.
     * @return Array
     */
    public function toArray()
    {
        return $this->attributes;
    }

    /**
     * This method performs a validation via ApiValidator class. It takes a
     * scenario's rules and calls apropriate validator's method.
     * @param String $scenario
     * @return bool
     */
    protected function validate($scenario)
    {
        if (isset($this->rules[$scenario])) {
            $rules = $this->rules[$scenario];
            return $this->validator->validate($this->attributes, $rules);
        }
        return false;
    }
}
