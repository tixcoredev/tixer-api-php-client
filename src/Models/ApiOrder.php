<?php
/**
 * Tixer Api
 *
 * PHP version 5.6
 *
 * @category   Tixer Api
 * @package    Tixer
 * @subpackage TixerApiClient/Models
 * @author     Dariusz Wiśniewski <kontakt@darks.pl>
 * @license    http://tixer.pl/ MIT
 * @link       http://tixer.pl/
 */
namespace TixerApiClient\Models;

/**
 * ApiOrder class
 */
class ApiOrder extends ApiModel
{
    protected $attributes = ['id', 'name', 'address', 'postal_code', 'city', 'country', 'phone', 'shipment', 'status'];

    /**
     * This method creates an order from currently reserved items.
     * API operation: /Order/group (get/post if shipment)
     * @param ApiShipment|null $shipment
     * @return ApiOrder
     */
    public function create($shipment = null)
    {
        if (is_null($shipment)) {
            $this->attributes = $this->apiClient->get('Order/group', true, $this->sub);
        } else if ($shipment->validate()) {
            $this->attributes = $this->apiClient->post('Order/group', $shipment->toArray(), true, $this->sub);
        }
        return $this;
    }
}
