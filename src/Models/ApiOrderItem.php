<?php
/**
 * Tixer Api
 *
 * PHP version 5.6
 *
 * @category   Tixer Api
 * @package    Tixer
 * @subpackage TixerApiClient/Models
 * @author     Dariusz Wiśniewski <kontakt@darks.pl>
 * @license    http://tixer.pl/ MIT
 * @link       http://tixer.pl/
 */
namespace TixerApiClient\Models;

/**
 * ApiOrderItem class
 */
class ApiOrderItem extends ApiModel
{
    protected $attributes = ['id', 'item_id', 'item_class', 'quantity', 'order', 'user_id', 'weight', 'shipment_type'];
    protected $rules = [
        'create' => [
            'item_id' => 'required|numeric',
            'item_class' => 'required|string',
            'quantity' => 'required|numric',
            'shipment' => 'required|string'
        ]
    ];

    /**
     * This mehod creates (reserves) a signle order item.
     * API operation: /Order/add (post)
     * @param Array $attributes
     * @return ApiOrderItem
     */
    public function create($attributes = [])
    {
        if ($this->validate('create')) {
            $this->attributes = $this->apiClient->post('Order/add', $attributes, true, $this->sub);
        }
        return $this;
    }

    /**
     * This method returns an array of all order items that are currently in the cart
     * API operation: /Order/cart (get)
     * @return Array of ApiOrderItem
     */
    public function all()
    {
        $cartItems = [];
        $orderItemsAttributes = $this->apiClient->get('Order/cart', true, $this->sub);
        foreach ($orderItemsAttributes as $orderItemAttributes) {
            array_push($cartItems, new self($this->apiClient, $orderItemAttributes));
        }
        return $cartItems;
    }

    /**
     * This method deletes specified order items.
     * API operation: /Order/remove (post)
     * @param Array $ids
     * @return Void
     */
    public function delete($ids)
    {
        $this->apiClient->post('Order/remove', $ids, true, $this->sub);
    }

    /**
     * This method deletes all not grouped order items.
     * API operation: /Order/clear (get)
     * @return Void
     */
    public function deleteAll()
    {
        $this->apiClient->get('Order/clear', true, $this->sub);
    }
}
