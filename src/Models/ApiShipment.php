<?php
/**
 * Tixer Api
 *
 * PHP version 5.6
 *
 * @category   Tixer Api
 * @package    Tixer
 * @subpackage TixerApiClient/Models
 * @author     Dariusz Wiśniewski <kontakt@darks.pl>
 * @license    http://tixer.pl/ MIT
 * @link       http://tixer.pl/
 */
namespace TixerApiClient\Models;

/**
 * ApiShipment class
 */
class ApiShipment extends ApiModel
{
    protected $attributes = ['id', 'name', 'days', 'addy', 'phone', 'weight'];

    protected $rules = [
        'create' => [
            'method' => 'required|numeric',
            'name' => 'required|string',
            'address' => 'required|string',
            'postal_code' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'phone' => 'required|string'
        ]
    ];

    /**
     * This method returns all available shipemnt options for current cart.
     * API operation: /Order/shipments (get)
     * @return Array of ApiShipment
     */
    public function available()
    {
        $availabelShipments = [];
        $availabelShipmentsAttributes = $this->apiClient->get('/Order/shipments', true);
        foreach ($availabelShipmentsAttributes as $availabelShipmentsAttribute) {
            array_push($availabelShipments, new self($this->apiClient, $availabelShipmentsAttribute));
        }
        return $availabelShipments;
    }
}
