<?php
/**
 * Tixer Api
 *
 * PHP version 5.6
 *
 * @category   Tixer Api
 * @package    Tixer
 * @subpackage TixerApiClient
 * @author     Dariusz Wiśniewski <kontakt@darks.pl>
 * @license    http://tixer.pl/ MIT
 * @link       http://tixer.pl/
 */
namespace TixerApiClient;

/**
 *  ApiValidator class
 */
class ApiValidator
{
    private $attributes;
    private $rules;
    private $messages = [
        'required' => 'Field {field} is required.',
        'numeric' => 'Field {field} supposed to be a number.',
        'string' => 'Field {field} supposed to be a string'
    ];
    private $errorMessages;

    /**
     *  This method performs a validation across given atrributes checking
     * all given values.
     * @param Array $attributes
     * @param Array $rules
     * @return bool
     */
    public function validate($attributes, $rules)
    {
        $this->attributes = $attributes;
        $this->rules = $rules;
        foreach ($this->attributes as $attribute) {
            if (isset($this->rules[$attribute])) {
                $rules = explode('|', $this->rules[$attribute]);
                foreach ($rules as $rule) {
                    if (method_exists($this, $rule)) {
                        if (!$this->$rule($attribute)) {
                            $this->errorMessages[$attribute] = isset($this->messages[$rule])
                                ? str_replace('{field}', $attribute, $this->messages[$rule])
                                : '';
                        }
                    }
                }
            }
        }
        return empty($this->errorMessages);
    }

    /**
     *  This method returns all error messages from the validation procedure.
     * @return Array
     */
    public function getMessages()
    {
        return $this->errorMessages;
    }

    /**
     *  This is a validation method that checks if attribute is set.
     * @param Mixed $attribute
     * @return bool
     */
    private function required($attribute)
    {
        return !empty($attribute);
    }

    /**
     *  This is a validation method that checks if attribute's type is numeric.
     * @param Mixed $attribute
     * @return bool
     */
    private function numeric($attribute)
    {
        return is_numeric($attribute);
    }

    /**
     *  This is a validation method that checks if attribute's type is string.
     * @param Mixed $attribute
     * @return bool
     */
    private function string($attribute)
    {
        return is_string($attribute);
    }
}
