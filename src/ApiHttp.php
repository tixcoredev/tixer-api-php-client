<?php
/**
 * Tixer Api
 *
 * PHP version 5.6
 *
 * @category   Tixer Api
 * @package    Tixer
 * @subpackage TixerApiClient
 * @author     Dariusz Wiśniewski <kontakt@darks.pl>
 * @license    http://tixer.pl/ MIT
 * @link       http://tixer.pl/
 */
namespace TixerApiClient;

use GuzzleHttp\Client;

/**
 *  ApiHttp class
 */
class ApiHttp
{
    private $client;
    private $headers = [
        'Content-Type' => 'application/json',
        'X-Client' => 'PHP',
        'Accept' => 'application/json'
    ];

    /**
     *  This contructor requires a base URL of the API
     * @param String $url
     * @return Void
     */
    public function __construct($url)
    {
        $this->headers['base_uri'] = $url;
    }

    /**
     *  This method returns a Guzzle Http client object.
     * @return Void
     */
    protected function getClient()
    {
        return ($this->client === null) ? $this->client = new Client($this->headers) : $this->client;
    }

    /**
     *  This method performs request on guzzle http client.
     * @param String $type
     * @param String $action
     * @param array $data
     * @return PSR7 Response
     */
    public function request($type, $action, $data = [])
    {
        $options = ['headers' => $this->getHeaders()];
        if (!empty($data)) {
            $options['json'] = $data;
        }

        return $this->getClient()->request($type, $action, $options);
    }

    /**
     *  This method allows to set addtional request header.
     * @param String $headerName
     * @param String $headerValue
     * @return ApiHttp object
     */
    public function addHeader($headerName, $headerValue)
    {
        $this->headers[$headerName] = $headerValue;
        return $this;
    }

    /**
     *  This method returns all http headers for currect request.
     * @return Array
     */
    private function getHeaders()
    {
        return $this->headers;
    }
}
