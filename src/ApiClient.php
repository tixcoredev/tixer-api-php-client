<?php
/**
 * Tixer Api
 *
 * PHP version 5.6
 *
 * @category   Tixer Api
 * @package    Tixer
 * @subpackage TixerApiClient
 * @author     Dariusz Wiśniewski <kontakt@darks.pl>
 * @license    http://tixer.pl/ MIT
 * @link       http://tixer.pl/
 */
namespace TixerApiClient;

use TixerApiClient\ApiHttp;
use TixerApiClient\ApiAuth;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;

/**
 *  ApiClient class
 */
class ApiClient
{
    private $http;
    private $auth;
    private $invalidTokenCallback;
    private $signer;
    private $token;

    /**
     *  This contructor requires a base URL of the API
     * @param String $url
     * @return Void
     */
    public function __construct($url)
    {
        $this->http = new ApiHttp($url);
        $this->signer = new Sha256();
    }

    /**
     * This method sets Authentication token gained from ApiAuth's login method.
     * It's helpfull when you would like to store API credentials for JWT
     * in external database (outside the plugin). Tixer API is some cases
     * (like for admin account) sets particular timeout and therefore there's
     * no need to authenticate before each request.
     * @param Array $token
     * @return Void
     */
    public function setAuthToken($token)
    {
        $this->token = $token;
    }

    /**
     *  This method returns role of the currently uthenticated user.
     * @return String
     */
    public function getRole()
    {
        return isset($this->token['role_txt']) ? $this->token['role_txt'] : false;
    }

    /**
     *  Ths method allows you to set callback method that is invoked when
     * 401 authentication error occure. This is usefull when you would like to
     * get a new token and delete previous one in you external (app) database.
     * @param Callable $callable
     * @return Void
     */
    public function setInvalidTokenCallback($callable)
    {
        $this->invalidTokenCallback = $callable;
    }

    /**
     *  This method performs get request via guzelle http client. Additionally
     * it may add authoriation header based on JWT standard.
     * @param String $action
     * @param bool $auth
     * @param numeric|null $sub
     * @return Array $responseBody
     */
    public function get($action, $auth = false, $sub = null)
    {
        if ($auth) {
            $this->authorize($sub);
        }
        return $this->httpRequest('GET', $action);
    }

    /**
     *  This method performs post request via guzelle http client. Additionally
     * it may add authoriation header based on JWT standard.
     * @param String $action
     * @param array $data
     * @param bool $auth
     * @param numeric|null $sub
     * @return Array $responseBody
     */
    public function post($action, $data = [], $auth = false, $sub = null)
    {
        if ($auth) {
            $this->authorize($sub);
        }
        return $this->httpRequest('POST', $action, $data);
    }

    /**
     *  This method performs put request via guzelle http client. Additionally
     * it may add authoriation header based on JWT standard.
     * @param String $action
     * @param array $data
     * @param bool $auth
     * @param numeric|null $sub
     * @return Array $responseBody
     */
    public function put($action, $data = [], $auth = false, $sub = null)
    {
        if ($auth) {
            $this->authorize($sub);
        }
        return $this->httpRequest('PUT', $action, $data);
    }

    /**
     *  This method performs delete request via guzelle http client. Additionally
     * it may add authoriation header based on JWT standard.
     * @param String $action
     * @param bool $auth
     * @param numeric|null $sub
     * @return Array $responseBody
     */
    public function delete($action, $auth = false, $sub = null)
    {
        if ($auth) {
            $this->authorize($sub);
        }
        return $this->httpRequest('DELETE', $action);
    }

    /**
     *  This method performs the (direct) request on to the guzelle client
     * and nadle the response via hadleResponse method.
     * @param String $type
     * @param String $action
     * @param array $data
     * @return Array $responseBody
     */
    public function httpRequest($type, $action, $data = [])
    {
        try {
            $httpResponse = $this->http->request($type, $action, $data);
            return $this->handleResponse($httpResponse);
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            return $this->handleResponse($exception->getResponse());
        }
    }

    /**
     *  This method handle the response from guzzle http client. It checks
     * response code and accordingly returns value or throws an exception.
     * @param PSR7 Response $response
     * @return Array $responseBody
     */
    private function handleResponse($response)
    {
        $responseCode = $response->getStatusCode();
        $responseContent = json_decode($response->getBody()->getContents());
        switch ($responseCode) {
            case 200:
                return json_decode(json_encode($responseContent), true);
            case 401:
                if (is_callable($this->invalidTokenCallback)) {
                    call_user_func($this->invalidTokenCallback);
                }
                //Go to default if no callback
            default:
                throw new \Exception($responseCode . ': ' . $responseContent->message);
        }
    }

    /**
     *  This method generates JWT token and sets it as additional http header.
     * @param numeric|null $sub
     * @return Void
     */
    private function authorize($sub = null)
    {
        $this->http->addHeader('Authorization', 'Bearer ' . $this->getBearer($sub));
    }

    /**
     *  This method generates JWT token for specified header, claim and optional
     * sub. This is based on external library Lcobucci\JWT and uses Builder to
     * generate new Bearer. It needs $this->token to be available (You can
     * set its value via $this->setAuthToken method).
     * @param type|null $sub
     * @return type
     */
    public function getBearer($sub = null)
    {
        $token = null;
        if (isset($this->token->header, $this->token->claim, $this->token->token)) {
            $bearer = (new Builder())->setHeader('alg', $this->token->header->alg);
            $bearer->setHeader('typ', $this->token->header->typ);
            $bearer->set('iat', time());

            foreach ($this->token->claim as $claimName => $claimValue) {
                $bearer->set($claimName, $claimValue);
            }

            if (!is_null($sub)) {
                $bearer->set('sub', (string)$sub);
            }
            $bearer->sign($this->signer, new Key($this->token->token));
            $token = $bearer->getToken();
        }

        return $token;
    }
}
