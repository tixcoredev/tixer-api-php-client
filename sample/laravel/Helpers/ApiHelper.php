<?php
/**
 * String
 *
 * PHP version 5.6
 *
 * @category   Tixer
 * @package    TiXER
 * @subpackage Utility
 * @author     Dariusz Wiśniewski <kontakt@darks.pl>
 * @license    http://tixer.pl/ MIT
 * @link       http://tixer.pl/
 */

namespace App\Helpers;

use TixerApiClient\ApiClient;
use TixerApiClient\Models\ApiAuth;
use App\Models\ApiToken;

class ApiHelper
{
    private static $atemptsToLogin = 0;

    public static function getApiClient()
    {
        $apiClient = new ApiClient(env('NEW_API_URL', 'http://tixer.tixcore.com.pl:8080'));
        $apiClient->setAuthToken(self::getApiToken($apiClient));
        $apiClient->setInvalidTokenCallback('App\Helpers\ApiHelper::invalidTokenCallback');
        return $apiClient;
    }

    public static function invalidTokenCallback()
    {
        self::deleteApiToken();
    }

    public static function getApiCredentials()
    {
        return [
            'email' => env('NEW_API_USER'),
            'password' => env('NEW_API_SECRET')
        ];
    }

    public static function login($apiClient)
    {
        self::$atemptsToLogin++;
        $apiAuth = new ApiAuth($apiClient);
        $user = \Auth::user();
        if (isset($user->facebook_token) && !empty($user->facebook_token)) {
            $apiAuth->loginByFacebook($user->facebook_token);
        } else {
            $apiCredentials = self::getApiCredentials();
            $apiAuth->login($apiCredentials['email'], $apiCredentials['password']);
        }
        return $apiAuth;
    }

    public static function getApiToken($apiClient)
    {
        $currentToken = self::selectApiToken();
        if (empty($currentToken)) {
            $currentToken = self::getNewApiToken($apiClient);
            self::insertApiToken($currentToken);
        }
        return $currentToken;
    }

    public static function getNewApiToken($apiClient)
    {
        $apiAuth = self::login($apiClient);
        return empty($apiAuth) ? [] : json_decode(json_encode($apiAuth->token));
    }

    public static function selectApiToken()
    {
        $user = \Auth::user();
        if (isset($user->facebook_token) && !empty($user->facebook_token)) {
            $apiToken = ApiToken::where('fb_token', $user->facebook_token)->first();
        } else {
            $apiCredentials = self::getApiCredentials();
            $apiToken = ApiToken::where('email', $apiCredentials['email'])->first();
        }
        return empty($apiToken) ? [] : json_decode($apiToken->token);
    }

    public static function insertApiToken($token)
    {
        $user = \Auth::user();
        if (isset($user->facebook_token) && !empty($user->facebook_token)) {
            $apiToken = ApiToken::firstOrCreate(['fb_token'=> $user->facebook_token]);
        } else {
            $apiCredentials = self::getApiCredentials();
            $apiToken = ApiToken::firstOrCreate(['email' => $apiCredentials['email']]);
        }
        return empty($apiToken) ? false : $apiToken->update(['token' => json_encode($token)]);
    }

    public static function deleteApiToken()
    {
        $user = \Auth::user();
        if (isset($user->facebook_token) && !empty($user->facebook_token)) {
            $apiToken = ApiToken::where('fb_token', $user->facebook_token)->delete();
        } else {
            $apiCredentials = self::getApiCredentials();
            $apiToken = ApiToken::where('email', $apiCredentials['email'])->delete();
        }
    }
}
