<?php
/**
 * Client
 *
 * PHP version 5.6
 *
 * @category   Model
 * @package    TiXER
 * @subpackage ApiToken
 * @author     Dariusz Wiśniewski <kontakt@darks.pl>
 * @license    http://tixer.pl/ MIT
 * @link       http://tixer.pl/
 */
namespace App\Models;
use TixerApiClient\Models\ApiOrder;

class ApiToken extends BaseModel
{

    protected $fillable = [
        'email',
        'fb_token',
        'token'
    ];
}
